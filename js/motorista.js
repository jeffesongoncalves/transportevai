$('form').validate({
  rules:{
    txtnome:{
      required:true
    },
    txtfuncao:{
      required:true
    },
    txtcel:{
      required:true
    },
    txtfone:{
      required:true
    },
    txtemail:{
      required:true
    }
  },
  messages:{
    txtnome:{
      required:"Campo Nome Obrigatorio!"
    },
    txtfuncao:{
      required:"Campo Função Obrigatorio!"
    },
    txtcel:{
      required:"Campo Telefone Obrigatorio!"
    },
    txtfone:{
      required:"Campo Telefone Fixo Obrigatorio!"
    },
    txtemail:{
      required:"Campo Email Obrigatorio!"
    }
  }

})


var site = "http://104.236.122.55:80/doctum/pw/tp1/route.php";


function inserir(){
	$.ajax({
		type: 'POST',
		url: site,
		datatype: "jsonp",
		data: JSON.parse({
			nome: $("txtnome").val(),
			telefone_fixo: $("txtfone").val(),
			telefone_movel: $("txtcel").val(),
			email: $("txtemail").val(),
		}),
		headers: {
			"table": "funcionario",		
		},
		success : function(data) {
			alert("Enviado!");
			console.log(data);
		},
		error : function(error) {
			alert("Deu Erro!");
			console.log("Erro: " + error);
		}
	});
}

$(document).ready(function(){
	$.ajax({
		type:"GET",
		url: site,
		datatype: "jsonp",
		headers:{
			"table":"motorista",
		},
		contentype: "application/json",
		success:function(result){
			atualizar(JSON.parse(result));
		},
		error: function(erro){
			alert("Deu erro!");
		}
	})
});

function atualizar(fun){
	var tabela =  document.getElementById("tabelaDados");
	tabela.innerHTML = "";
	for (var i = fun.length - 1; i >= 0; i--) {

		var linha = document.createElement("tr");

		var cid = document.createElement("td");
		var cnome = document.createElement("td");
		var ctelefone_fixo = document.createElement("td");
		var ctelefone_movel = document.createElement("td");
		var cemail = document.createElement("td");
		var ceditar = document.createElement("td");
		var cexcluir = document.createElement("td");

		var vid = document.createTextNode(fun[i].id);
		var vnome = document.createTextNode(fun[i].nome);
		var vtelefone_fixo = document.createTextNode(fun[i].telefone_fixo);
		var vtelefone_movel = document.createTextNode(fun[i].telefone_movel);
		var vemail = document.createTextNode(fun[i].email);
		ceditar.innerHTML = '<a class="btn btn-success">Editar</a>';
		cexcluir.innerHTML = '<a class="btn btn-danger">Excluir</a>';



		cid.appendChild(vid);
		cnome.appendChild(vnome);
		ctelefone_fixo.appendChild(vtelefone_fixo);
		ctelefone_movel.appendChild(vtelefone_movel);
		cemail.appendChild(vemail);

		linha.appendChild(cid);
		linha.appendChild(cnome);
		linha.appendChild(ctelefone_fixo);
		linha.appendChild(ctelefone_movel);
		linha.appendChild(cemail);
		linha.appendChild(ceditar);
		linha.appendChild(cexcluir);
		tabela.appendChild(linha);


	}
}
